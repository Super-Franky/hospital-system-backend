package com.hospitalsystem.hospitalsystembackend.controller;

import com.hospitalsystem.hospitalsystembackend.common.BaseContext;
import com.hospitalsystem.hospitalsystembackend.common.R;
import com.hospitalsystem.hospitalsystembackend.domain.Appointment;
import com.hospitalsystem.hospitalsystembackend.domain.PatientAppointment;
import com.hospitalsystem.hospitalsystembackend.service.AppointmentService;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("appointment")
public class AppointmentController {

    private final AppointmentService appointmentService;

    AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    /**
     * 新增预约
     */
    @PostMapping("")
    public R<String> createAppointment(@RequestBody Appointment appointment) {
        try {
            appointment.setDoctorId(BaseContext.getCurrentId());
            appointmentService.create(appointment);
            return R.success("生成预约成功");
        } catch (Exception e) {
            return R.error("生成预约失败");
        }
    }

    /**
     * 进行预约
     */
    @PostMapping("makeAppointment")
    public R<String> makeAppointment(@RequestBody PatientAppointment patientAppointment) {
        try {
            // 设置进行预约的病人是当前用户
            patientAppointment.setPatientId(BaseContext.getCurrentId());
            return appointmentService.makeAppointment(patientAppointment);
        } catch (Exception e) {
            return R.error("预约失败");
        }
    }
}
