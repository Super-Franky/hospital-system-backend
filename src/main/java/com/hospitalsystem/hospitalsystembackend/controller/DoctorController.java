package com.hospitalsystem.hospitalsystembackend.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hospitalsystem.hospitalsystembackend.common.BaseContext;
import com.hospitalsystem.hospitalsystembackend.common.R;
import com.hospitalsystem.hospitalsystembackend.domain.Appointment;
import com.hospitalsystem.hospitalsystembackend.domain.Doctor;
import com.hospitalsystem.hospitalsystembackend.service.AppointmentService;
import com.hospitalsystem.hospitalsystembackend.service.DoctorService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("doctor")
public class DoctorController {

    private final DoctorService doctorService;
    private final AppointmentService appointmentService;

    DoctorController(DoctorService doctorService, AppointmentService appointmentService) {
        this.doctorService = doctorService;
        this.appointmentService = appointmentService;
    }

    /**
     * 登录
     */
    @PostMapping("login")
    public R<Doctor> login(HttpServletRequest request, @RequestBody Doctor doctor) {
        try {
            String openId = request.getHeader("x-wx-openid");
            doctor.setOpenId(openId);
            Doctor user_doctor = doctorService.login(doctor);
            // 将用户登录状态存储在session中，方便下次存取
            request.getSession().setAttribute("doctor", user_doctor.getId());
            return R.success(user_doctor);
        } catch (Exception e) {
            return R.error("登录失败！");
        }
    }

    /**
     * 修改信息
     */
    @PutMapping("update")
    public R<Doctor> update(@RequestBody Doctor doctor) {
        try {
            // 设置要更新的医生用户id为当前登录用户id
            doctor.setId(BaseContext.getCurrentId());
            Doctor updated_doctor = doctorService.update(doctor);
            return R.success(updated_doctor);
        } catch (Exception e) {
            return R.error("更新个人信息失败");
        }
    }

    /**
     * 获取预约信息
     */
    @GetMapping("appointmentPage")
    public R<Page<Appointment>> getAppointmentPage(int page, int pageSize) {
        try {
            Page<Appointment> appointmentPage = appointmentService
                    .getDoctorAppointmentPage(page, pageSize, BaseContext.getCurrentId());
            return R.success(appointmentPage);
        } catch (Exception e) {
            return R.error("获取预约失败");
        }
    }
}
