package com.hospitalsystem.hospitalsystembackend.controller;


import com.hospitalsystem.hospitalsystembackend.common.BaseContext;
import com.hospitalsystem.hospitalsystembackend.common.R;
import com.hospitalsystem.hospitalsystembackend.domain.Patient;
import com.hospitalsystem.hospitalsystembackend.service.PatientService;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("patient")
public class PatientController {

    private final PatientService patientService;

    PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    /**
     * 登录
     */
    @PostMapping("login")
    public R<Patient> login(HttpServletRequest request, @RequestBody Patient patient) {
        try {
            String openId = request.getHeader("x-wx-openid");
            patient.setOpenId(openId);
            Patient user_patient = patientService.login(patient);
            // 将用户登录状态存储在session中，方便下次存取
            request.getSession().setAttribute("patient", user_patient.getId());
            return R.success(user_patient);
        } catch (Exception e) {
            return R.error("登录失败！");
        }
    }

    /**
     * 修改信息
     */
    @PutMapping("update")
    public R<Patient> update(@RequestBody Patient patient) {
        try {
            // 设置要更新的病人用户id为当前登录用户id
            patient.setId(BaseContext.getCurrentId());
            Patient updated_patient = patientService.update(patient);
            return R.success(updated_patient);
        } catch (Exception e) {
            return R.error("更新个人信息失败");
        }
    }
}
