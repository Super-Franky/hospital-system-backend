package com.hospitalsystem.hospitalsystembackend.filter;

import com.alibaba.fastjson.JSON;
import com.hospitalsystem.hospitalsystembackend.common.BaseContext;
import com.hospitalsystem.hospitalsystembackend.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 检查用户是否已完成登录
 */
@WebFilter(filterName = "loginCheckFilter", urlPatterns = "/*")
@Slf4j
public class LoginCheckFilter implements Filter {

    // 路径匹配器
    private static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    // 不需要被处理的请求路径
    private static final String[] urls = new String[]{
            "/doctor/getId",
            "/patient/getId",
            "/doctor/login",
            "/patient/login",
            "/doctor/logout",
            "/patient/logout"
    };

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String requestURI = request.getRequestURI();
        log.info("拦截到请求：{}", requestURI);

        boolean check = check(requestURI);

        if (check) {
            log.info("本次请求{}不需要处理", requestURI);
            filterChain.doFilter(request,response);
            return;
        }

        if (request.getSession().getAttribute("doctor") != null) {
            log.info("用户已登录，用户id为：{}",request.getSession().getAttribute("doctor"));
            BaseContext.setCurrentId((Integer) request.getSession().getAttribute("doctor"));
            filterChain.doFilter(request,response);
            return;
        }

        if (request.getSession().getAttribute("patient") != null) {
            log.info("用户已登录，用户id为：{}",request.getSession().getAttribute("patient"));
            BaseContext.setCurrentId((Integer) request.getSession().getAttribute("patient"));
            filterChain.doFilter(request,response);
            return;
        }

        log.info("用户未登录");
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
    }

    private boolean check(String requestURI) {
        for (String url : urls) {
            boolean match = PATH_MATCHER.match(url, requestURI);
            if(match){
                return true;
            }
        }
        return false;
    }
}