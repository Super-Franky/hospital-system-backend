package com.hospitalsystem.hospitalsystembackend.mapper;

import com.hospitalsystem.hospitalsystembackend.domain.Appointment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;

/**
* @author 0000
* @description 针对表【appointment】的数据库操作Mapper
* @createDate 2022-10-08 16:01:32
* @Entity com.hospitalsystem.hospitalsystembackend.domain.Appointment
*/
public interface AppointmentMapper extends BaseMapper<Appointment> {

    Appointment selectByIdForUpdate(@Param("id") Serializable id);

}




