package com.hospitalsystem.hospitalsystembackend.mapper;

import com.hospitalsystem.hospitalsystembackend.domain.Doctor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author 0000
* @description 针对表【doctor】的数据库操作Mapper
* @createDate 2022-10-06 14:56:50
* @Entity com.hospitalsystem.hospitalsystembackend.domain.Doctor
*/
public interface DoctorMapper extends BaseMapper<Doctor> {

}




