package com.hospitalsystem.hospitalsystembackend.mapper;

import com.hospitalsystem.hospitalsystembackend.domain.PatientAppointment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.io.Serializable;

/**
* @author 0000
* @description 针对表【patient_appointment】的数据库操作Mapper
* @createDate 2022-10-06 14:56:50
* @Entity com.hospitalsystem.hospitalsystembackend.domain.PatientAppointment
*/
public interface PatientAppointmentMapper extends BaseMapper<PatientAppointment> {

    PatientAppointment selectByPidAndAidForUpdate(@Param("patientId") Serializable patientId
            , @Param("appointmentId") Serializable appointmentId);

}




