package com.hospitalsystem.hospitalsystembackend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@MapperScan("com.hospitalsystem.hospitalsystembackend.mapper")
@ServletComponentScan
public class HospitalSystemBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospitalSystemBackendApplication.class, args);
    }

}
