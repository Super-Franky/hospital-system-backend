package com.hospitalsystem.hospitalsystembackend.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hospitalsystem.hospitalsystembackend.common.R;
import com.hospitalsystem.hospitalsystembackend.domain.Appointment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hospitalsystem.hospitalsystembackend.domain.PatientAppointment;

/**
* @author 0000
* @description 针对表【appointment】的数据库操作Service
* @createDate 2022-10-08 16:01:32
*/
public interface AppointmentService extends IService<Appointment> {

    void create(Appointment appointment) throws Exception;

    R<String> makeAppointment(PatientAppointment patientAppointment);

    Page<Appointment> getDoctorAppointmentPage(int page, int pageSize, int doctorId);
}
