package com.hospitalsystem.hospitalsystembackend.service;

import com.hospitalsystem.hospitalsystembackend.domain.Doctor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 0000
* @description 针对表【doctor】的数据库操作Service
* @createDate 2022-10-06 14:56:50
*/
public interface DoctorService extends IService<Doctor> {

    Doctor login(Doctor doctor);

    Doctor update(Doctor doctor) throws Exception;
}
