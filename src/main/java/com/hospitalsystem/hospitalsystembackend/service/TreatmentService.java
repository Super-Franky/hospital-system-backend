package com.hospitalsystem.hospitalsystembackend.service;

import com.hospitalsystem.hospitalsystembackend.domain.Treatment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 0000
* @description 针对表【treatment】的数据库操作Service
* @createDate 2022-10-06 14:56:50
*/
public interface TreatmentService extends IService<Treatment> {

}
