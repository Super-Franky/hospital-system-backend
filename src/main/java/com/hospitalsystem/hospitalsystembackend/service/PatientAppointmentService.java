package com.hospitalsystem.hospitalsystembackend.service;

import com.hospitalsystem.hospitalsystembackend.domain.PatientAppointment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.Serializable;

/**
* @author 0000
* @description 针对表【patient_appointment】的数据库操作Service
* @createDate 2022-10-06 14:56:50
*/
public interface PatientAppointmentService extends IService<PatientAppointment> {

    PatientAppointment getOneByPidAndAidForUpdate(Serializable patientId, Serializable appointmentId);

}
