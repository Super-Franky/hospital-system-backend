package com.hospitalsystem.hospitalsystembackend.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospitalsystem.hospitalsystembackend.domain.PatientAppointment;
import com.hospitalsystem.hospitalsystembackend.service.PatientAppointmentService;
import com.hospitalsystem.hospitalsystembackend.mapper.PatientAppointmentMapper;
import org.springframework.stereotype.Service;

import java.io.Serializable;

/**
* @author 0000
* @description 针对表【patient_appointment】的数据库操作Service实现
* @createDate 2022-10-06 14:56:50
*/
@Service
public class PatientAppointmentServiceImpl extends ServiceImpl<PatientAppointmentMapper, PatientAppointment>
    implements PatientAppointmentService{

    @Override
    public PatientAppointment getOneByPidAndAidForUpdate(Serializable patientId, Serializable appointmentId) {
        return baseMapper.selectByPidAndAidForUpdate(patientId, appointmentId);
    }
}




