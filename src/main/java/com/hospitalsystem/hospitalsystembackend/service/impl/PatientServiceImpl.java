package com.hospitalsystem.hospitalsystembackend.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospitalsystem.hospitalsystembackend.domain.Patient;
import com.hospitalsystem.hospitalsystembackend.service.PatientService;
import com.hospitalsystem.hospitalsystembackend.mapper.PatientMapper;
import org.springframework.stereotype.Service;

/**
* @author 0000
* @description 针对表【patient】的数据库操作Service实现
* @createDate 2022-10-06 14:56:50
*/
@Service
public class PatientServiceImpl extends ServiceImpl<PatientMapper, Patient>
    implements PatientService{

    @Override
    public Patient login(Patient patient) {
        Patient user_patient = this.getOne(Wrappers.<Patient>lambdaQuery()
                .eq(Patient::getOpenId, patient.getOpenId()));
        if (user_patient == null) {
            // 用户在数据库中无记录，创建用户,并返回新创建的用户信息
            this.save(patient);
            return patient;
        } else {
            // 用户在数据库中有记录，直接返回的用户信息
            return user_patient;
        }
    }

    @Override
    public Patient update(Patient patient) throws Exception {
        if (patient == null) {
            // 用户在数据库中无记录
            throw new Exception("该用户不存在");
        } else {
            // 用户在数据库中有记录，直接返回的用户信息
            this.updateById(patient);
            return patient;
        }
    }
}




