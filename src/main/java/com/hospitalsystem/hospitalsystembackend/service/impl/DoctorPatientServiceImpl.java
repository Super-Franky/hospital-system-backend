package com.hospitalsystem.hospitalsystembackend.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospitalsystem.hospitalsystembackend.domain.DoctorPatient;
import com.hospitalsystem.hospitalsystembackend.service.DoctorPatientService;
import com.hospitalsystem.hospitalsystembackend.mapper.DoctorPatientMapper;
import org.springframework.stereotype.Service;

/**
* @author 0000
* @description 针对表【doctor_patient】的数据库操作Service实现
* @createDate 2022-10-06 14:56:50
*/
@Service
public class DoctorPatientServiceImpl extends ServiceImpl<DoctorPatientMapper, DoctorPatient>
    implements DoctorPatientService{

}




