package com.hospitalsystem.hospitalsystembackend.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospitalsystem.hospitalsystembackend.common.BaseContext;
import com.hospitalsystem.hospitalsystembackend.common.R;
import com.hospitalsystem.hospitalsystembackend.domain.Appointment;
import com.hospitalsystem.hospitalsystembackend.domain.Doctor;
import com.hospitalsystem.hospitalsystembackend.domain.PatientAppointment;
import com.hospitalsystem.hospitalsystembackend.service.AppointmentService;
import com.hospitalsystem.hospitalsystembackend.mapper.AppointmentMapper;
import com.hospitalsystem.hospitalsystembackend.service.DoctorService;
import com.hospitalsystem.hospitalsystembackend.service.PatientAppointmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* @author 0000
* @description 针对表【appointment】的数据库操作Service实现
* @createDate 2022-10-08 16:01:32
*/
@Service
public class AppointmentServiceImpl extends ServiceImpl<AppointmentMapper, Appointment>
    implements AppointmentService{

    private final DoctorService doctorService;
    private final PatientAppointmentService patientAppointmentService;

    AppointmentServiceImpl(DoctorService doctorService, PatientAppointmentService patientAppointmentService) {
        this.doctorService = doctorService;
        this.patientAppointmentService = patientAppointmentService;
    }

    @Override
    public void create(Appointment appointment) throws Exception {
        if (appointment.getTotalCount() <= 0) {
            throw new Exception("生成预约失败，请检查所填数据是否正确");
        }
        // 为预约设置标题
        Integer doctorId = appointment.getDoctorId();
        Doctor doctor = doctorService.getById(doctorId);
        String desc = doctor.getName() + "医生的预约";
        appointment.setDescription(desc);
        appointment.setDoctorId(doctorId);
        this.save(appointment);
    }

    @Override
    @Transactional
    public R<String> makeAppointment(PatientAppointment patientAppointment) {
        // 监测是否已经预约成功过
        PatientAppointment pa = patientAppointmentService
                .getOneByPidAndAidForUpdate(patientAppointment.getPatientId(), patientAppointment.getAppointmentId());
        if (pa == null) {
            // 未预约成功
            Appointment appointment = this.baseMapper.selectByIdForUpdate(patientAppointment.getAppointmentId());
            if (appointment.getUsedCount() < appointment.getTotalCount()) {
                // 还有号可以发放
                appointment.setUsedCount(appointment.getUsedCount() + 1);
                this.updateById(appointment);
                // 生成预约记录
                patientAppointment.setPatientId(BaseContext.getCurrentId());
                patientAppointment.setStatus(0);
                patientAppointmentService.save(patientAppointment);
                return R.success("预约成功");
            } else {
                // 预约人数已满
                return R.error("预约失败，人数已满");
            }
        } else {
            // 已有预约成功记录
            return R.error("已预约成功，请勿重复预约");
        }
    }

    @Override
    public Page<Appointment> getDoctorAppointmentPage(int page, int pageSize, int doctorId) {
        //构造分页构造器对象
        Page<Appointment> appointmentPage = new Page<>(page, pageSize);

        //条件构造器
        LambdaQueryWrapper<Appointment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Appointment::getDoctorId, doctorId);

        //执行分页查询
        this.page(appointmentPage, queryWrapper);
        return appointmentPage;
    }
}




