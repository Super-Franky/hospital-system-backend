package com.hospitalsystem.hospitalsystembackend.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospitalsystem.hospitalsystembackend.domain.Doctor;
import com.hospitalsystem.hospitalsystembackend.service.DoctorService;
import com.hospitalsystem.hospitalsystembackend.mapper.DoctorMapper;
import org.springframework.stereotype.Service;

/**
* @author 0000
* @description 针对表【doctor】的数据库操作Service实现
* @createDate 2022-10-06 14:56:50
*/
@Service
public class DoctorServiceImpl extends ServiceImpl<DoctorMapper, Doctor>
    implements DoctorService{

    @Override
    public Doctor login(Doctor doctor) {
        Doctor user_doctor = this.getOne(Wrappers.<Doctor>lambdaQuery()
                .eq(Doctor::getOpenId, doctor.getOpenId()));
        if (user_doctor == null) {
            // 用户在数据库中无记录，创建用户,并返回新创建的用户信息
            this.save(doctor);
            return doctor;
        } else {
            // 用户在数据库中有记录，直接返回的用户信息
            return user_doctor;
        }
    }

    @Override
    public Doctor update(Doctor doctor) throws Exception {
        if (doctor == null) {
            // 用户在数据库中无记录
            throw new Exception("该用户不存在");
        } else {
            // 用户在数据库中有记录，直接返回的用户信息
            this.updateById(doctor);
            return doctor;
        }
    }
}




