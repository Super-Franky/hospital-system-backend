package com.hospitalsystem.hospitalsystembackend.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hospitalsystem.hospitalsystembackend.domain.Treatment;
import com.hospitalsystem.hospitalsystembackend.service.TreatmentService;
import com.hospitalsystem.hospitalsystembackend.mapper.TreatmentMapper;
import org.springframework.stereotype.Service;

/**
* @author 0000
* @description 针对表【treatment】的数据库操作Service实现
* @createDate 2022-10-06 14:56:50
*/
@Service
public class TreatmentServiceImpl extends ServiceImpl<TreatmentMapper, Treatment>
    implements TreatmentService{

}




