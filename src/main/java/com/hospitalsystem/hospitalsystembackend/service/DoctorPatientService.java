package com.hospitalsystem.hospitalsystembackend.service;

import com.hospitalsystem.hospitalsystembackend.domain.DoctorPatient;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author 0000
* @description 针对表【doctor_patient】的数据库操作Service
* @createDate 2022-10-06 14:56:50
*/
public interface DoctorPatientService extends IService<DoctorPatient> {

}
